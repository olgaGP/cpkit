Pod::Spec.new do |s|
  s.name         =  'CPKit'
  s.version      =  '1.0.0'
  s.license      =  { :type => 'MIT', :file => 'LICENSE' }
  s.homepage     =  'https://olgaGP@bitbucket.org/olgaGP/cpkit'
  s.authors      =  { 'Olya Goncharuk' => 'olga.g@greenpandagames.com' }
  s.source       =  { :git => 'https://olgaGP@bitbucket.org/olgaGP/cpkit.git', :tag => s.version.to_s }

  s.platform              = :ios, '8.0'
  s.ios.deployment_target = '7.0'

  s.summary      =  'Cross promo Kit'
  s.description  =  'Cross promo Kit from Greempanda'

  s.source_files = "CPKit/*.{swift,h}"
end
