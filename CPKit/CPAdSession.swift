//
//  CPAdSession.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/10/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit

class CPAdSession: NSObject {
    var format: CPAdFormat = .undefined
    var creationDate: Date?
    var renderers: [CPAdPlacementType: [CPAdFormat:CPAdRendererProtocol]]?
    
    init(format: CPAdFormat, creationDate: Date) {
        self.format = format
        self.creationDate = creationDate
    }
    
    public func showAds(_ ads: [CPAdProtocol]?,
                        format: CPAdFormat?,
                        placementType: CPAdPlacementType?,
                        inContainerView containerView: UIView?) {
        
        guard let containerView = containerView,
            let theAds = ads,
            theAds.count > 0,
            let format = format,
            let placementType = placementType else { return }
        
        let renderer = rendererFor(placementType: placementType, format: format)
        let adsToRenderNumber = min(theAds.count, 4)
        if let renderedAds = renderer.ads {
            var validCappingAds = renderedAds.filter { $0.isValidCapping }
            if validCappingAds.count >= adsToRenderNumber { // all the ads have valid capping
                renderer.renderAds(ads: Array<CPAdProtocol>(validCappingAds[..<adsToRenderNumber]),
                                   forPlacement: placementType,
                                   inView: containerView)
            } else {
                //try to find ads with valid capping
                for ad in theAds {
                    if !adsContainsAd(ads: validCappingAds, ad: ad) && ad.isValidCapping {
                        validCappingAds.append(ad)
                    }
                }
                // if the number of ads is still not sufficient
                // invalidate cappings where it is possible
                if validCappingAds.count < adsToRenderNumber {
                    for var ad in theAds {
                        ad.invalidateCapping()
                    }
                }
                for ad in theAds {
                    if !adsContainsAd(ads: validCappingAds, ad: ad) && ad.isValidCapping {
                        validCappingAds.append(ad)
                    }
                }
                renderer.renderAds(ads: Array<CPAdProtocol>(validCappingAds[..<adsToRenderNumber]),
                                   forPlacement: placementType,
                                   inView: containerView)
            }
            
            
        } else {
            var validCappingAds = theAds.filter { $0.isValidCapping }
            if validCappingAds.count < adsToRenderNumber {
                if validCappingAds.count < adsToRenderNumber {
                    for var ad in theAds {
                        ad.invalidateCapping()
                    }
                }
                for ad in theAds {
                    if !adsContainsAd(ads: validCappingAds, ad: ad) && ad.isValidCapping {
                        validCappingAds.append(ad)
                    }
                }
            }
            renderer.renderAds(ads: Array<CPAdProtocol>(validCappingAds[..<adsToRenderNumber]),
                               forPlacement: placementType,
                               inView: containerView)
        }
    }
    
    
}

extension CPAdSession {
    
    private func rendererFor(placementType: CPAdPlacementType,
                             format: CPAdFormat) -> CPAdRendererProtocol {
        
        if var renderersForPlacementType = renderers?[placementType] {
            if let renderer = renderersForPlacementType[format] {
                return renderer
            } else {
                let renderer = createRendererForAdFormat(format)
                renderersForPlacementType[format] = renderer
                return renderer
            }
        } else {
            let renderer = createRendererForAdFormat(format)
            renderers = [placementType: [format : renderer]]
            
            return renderer
        }
    }
    
    private func createRendererForAdFormat(_ format: CPAdFormat) -> CPAdRendererProtocol {
        switch format {
        case .native:
            return CPAdRendererNative()
        case .square:
            return CPAdRendererSquare()
        case .fullScreen:
            return CPAdRendererFullscreen()
        default:
            return CPAdRendererNative()
        }
    }
    
    func adsContainsAd(ads: [CPAdProtocol], ad: CPAdProtocol) -> Bool {
        for anAd in ads {
            if anAd.id == ad.id {
                return true
            }
        }
        return false
    }
}


