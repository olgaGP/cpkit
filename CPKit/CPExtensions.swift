//
//  CPExtensions.swift
//  CPKit
//
//  Created by Olha Honcharuk on 7/3/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import Foundation

extension String {
    
    func inDocumentDirectoryURL() -> URL {
        var documentDirectoryURL = FileManager.default.urls(for: .documentDirectory,
                                                            in: .userDomainMask)[0]
        documentDirectoryURL.appendPathComponent("cpkit/files/")
        documentDirectoryURL.appendPathComponent(self)
        
        return documentDirectoryURL
    }
    
    func inDocumentDirectory() -> String {
        return inDocumentDirectoryURL().path
    }
    
    func stringByAddingPathExtension(_ pathExtension: String) -> String {
        return self + "." + pathExtension
    }
}
