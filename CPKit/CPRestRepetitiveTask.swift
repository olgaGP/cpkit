//
//  CPRestRepetitiveTask.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/7/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation

struct CPDownloadResourceRepetitiveTask: CPRepetitiveTaskProtocol {
    private var restManager: CPRestManager
    private var url: URL
    
    
    /// Input parameters for the Task. This should be adjusted for the actual Task
    /// For this example required input is in parameters
    init(restManager: CPRestManager, url: URL) {
        self.restManager = restManager
        self.url = url
        
    }
    
    func run(completion: @escaping CPRepetitiveTaskProtocolCompletion) -> Void {
        self.restManager.getData(fromURL: self.url) { (data, response, error) in
            guard let data = data  else {
                completion(CPRepetitiveTaskResult(error: CPRepetitiveTaskError.NoData))
                return
            }
            
            guard error == nil else  {
                guard let delay = (error as NSError?)?.userInfo["ErrorRetryDelayKey"] as? NSNumber  else {
                    completion(CPRepetitiveTaskResult(error: CPRepetitiveTaskError.Failed(error!)))
                    return
                }
                // request failed and can be retry later
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0,
                                              execute: {
                                                completion(CPRepetitiveTaskResult(error: CPRepetitiveTaskError.RetryDelay(delay.doubleValue)))
                })
                return
            }
            
            completion(CPRepetitiveTaskResult(success: data))
        }
    }
}
