//
//  CPRepetitiveTask.swift
//  CPKit
//
//  Created by Olha Honcharuk on 7/3/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import UIKit

import Foundation

public enum CPRepetitiveTaskError: Error {
    case Failed(Error)
    case RetryFailed(Int)
    case RetryDelay(Double)
    case NoData
    
    var isErrorWithDelay: Bool {
        switch (self) {
        case .RetryDelay:
            return true
        default:
            return false
        }
    }
}

public protocol CPRepetitiveTaskResultProtocol {
    associatedtype SuccessValue
    associatedtype Error
    
    init(success: SuccessValue)
    init(error: Error)
}

public enum CPRepetitiveTaskResult<T, Error>: CPRepetitiveTaskResultProtocol {
    case Success(T)
    case Failure(Error)
    
    public init(success: T) {
        self = .Success(success)
    }
    
    public init(error: Error) {
        self = .Failure(error)
    }
    
}

public typealias CPRepetitiveTaskProtocolCompletion = (CPRepetitiveTaskResult<Data, CPRepetitiveTaskError>) -> Void

public protocol CPRepetitiveTaskProtocol {
    func run(completion: @escaping CPRepetitiveTaskProtocolCompletion) -> Void
}

public extension CPRepetitiveTaskProtocol {
    func createRequestWithRetryAndRun(retryCount: Int,
                                      failure: @escaping (Error) -> Void,
                                      success: @escaping (Data) -> Void) {
        
        let operation = CPRequestWithRetry(retryCount: retryCount,
                                         failure: failure,
                                         success: success)
        operation.run(transientTask: self)
    }
}

// MARK: - RequestWithRetry

private class CPRequestWithRetry {
    private let retryCount: Int
    var currentRetry = 0
    private let failureCallback: (Error) -> Void
    private let successCallback: (Data) -> Void
    
    init(retryCount: Int,
         failure: @escaping (Error) -> Void ,
         success: @escaping (Data) -> Void) {
        
        self.retryCount = retryCount
        self.failureCallback = failure
        self.successCallback = success
    }
    
    func run(transientTask: CPRepetitiveTaskProtocol) {
        transientTask.run { (repetitiveTaskResult) -> Void in
            switch (repetitiveTaskResult) {
            case .Success(let value):
                self.successCallback(value)
            case .Failure(let error):
                // handle errors with retry or finish
                if !error.isErrorWithDelay {
                    self.failureCallback(error)
                } else if ((self.currentRetry + 1) > self.retryCount) {
                    self.currentRetry += 1
                    self.failureCallback(CPRepetitiveTaskError.RetryFailed(self.currentRetry))
                } else {
                    print("retry!")
                    self.run(transientTask: transientTask)
                }
            }
        }
    }
}

struct RepetitiveTask: CPRepetitiveTaskProtocol  {
    
    private var session: URLSession?
    private var url: URL
    
    
    /// Input parameters for the Task. This should be adjusted for the actual Task
    /// For this example required input is in parameters
    init(session: URLSession, url: URL, parameters: NSCoding?) {
        self.session = session
        self.url = url
        
    }
    
    func run(completion: @escaping CPRepetitiveTaskProtocolCompletion) -> Void {
        let sessionURLTask = session?.dataTask(with: self.url, completionHandler: { (data, response, error) in
            guard let data = data  else {
                completion(CPRepetitiveTaskResult(error: CPRepetitiveTaskError.NoData))
                return
            }
            
            guard error == nil else  {
                guard let delay = (error as NSError?)?.userInfo["ErrorRetryDelayKey"] as? NSNumber  else {
                    completion(CPRepetitiveTaskResult(error: CPRepetitiveTaskError.Failed(error!)))
                    return
                }
                // request failed and can be retry later
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0,
                                              execute: {
                                                completion(CPRepetitiveTaskResult(error: CPRepetitiveTaskError.RetryDelay(delay.doubleValue)))
                })
                return
            }
            
            completion(CPRepetitiveTaskResult(success: data))
        })
        sessionURLTask?.resume()
    }
}

////: FailableURLSession is mocked NSURLSession to randomly fail with random error
////: run multiple times to see different results
//
////let failableSession =  FailableURLSession(responseData: NSData())
//let sessionConfiguration = URLSessionConfiguration.default
//let session = URLSession.init(configuration: sessionConfiguration)
//let url = URL(string: "http://import-test.s3.wasabisys.com/square_AC_TEST_PROD_SQV_grandma__%40ITSf8gttc64XmPj0v6RWK_square_1557845770.mp4")
//let task = RepetitiveTask(session: session, url: url!, parameters: nil)
//
////: Run task maximum 3 times hoping for success
//
//task.createRequestWithRetryAndRun(retryCount: 3,
//                                  failure: { (error) -> Void in
//                                    print("failure \(error)")
//},
//                                  success: { (data) -> Void in
//                                    print("success \(data)")
//})
