//
//  URLDefines.swift
//  CPKit
//
//  Created by Olha Honcharuk on 7/1/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import Foundation
import UIKit

class CPDefines {
    static let PROD_URL = "http://crosspromotion.mangoo-games.com";
    static let PREPROD_URL = "http://xpromo.mangoo-games.com";
    static let CONTENT_URL = "\(PROD_URL)/api/v1/campaigns/placements";
    
    
//    static let URL_BASE = "https://crosspromo.greenpandagames.com/api/v2"
//    static let URL_BASE_PREPROD = "https://mangoobox-crosspromo.mangoo-games.com/api/v2"
//    static let CONTENT_URL = "\(URL_BASE)/campaigns/placements"
    
    static let applicationJSON = "application/json"
    static let contentType = "Content-Type"
    
    static var deviceUUID: String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    
    static var appId: String? {
        return "1"
    }
    
    static var country: String? {
        guard let code = (Locale.current as NSLocale).object(forKey: .countryCode) as? String else { return nil }
        return code
    }
    
    static var language: String {
        guard let lang = Locale.current.languageCode else { return "en" }
        return lang
    }
}
