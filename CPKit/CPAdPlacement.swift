//
//  CPAdPlacement.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/10/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation

@objc public enum CPAdPlacementType: Int {
    case gameStart = 0
    case gameEnd = 1
    case gameNew = 2
    case rewarded = 3
    case homescreen = 4
    case victoryscreen = 5
    case appStart = 6
    
    
    func key() -> String {
        switch self {
        case .gameStart:
            return "gameStart"
        case .gameEnd:
            return "gameEnd"
        case .gameNew:
            return "gameNew"
        case .rewarded:
            return "rewarded"
        case .homescreen:
            return "homescreen"
        case .victoryscreen:
            return "victoryscreen"
        case .appStart:
            return "appStart"
        
        default:
            return ""
        }
    }
    
    static func placementTypeFromKey(_ key: String) -> CPAdPlacementType {
        var modifiedKey = key.replacingOccurrences(of:"square_", with: "")
        modifiedKey = modifiedKey.replacingOccurrences(of: "native_", with: "")
        if modifiedKey == "game_start" { return .gameStart }
        if modifiedKey == "game_end" { return .gameEnd }
        if modifiedKey == "game_new" { return .gameNew }
        if modifiedKey == "rewarded" { return .rewarded }
        if modifiedKey == "homescreen" { return .homescreen }
        if modifiedKey == "victoryscreen" { return .victoryscreen }
        if modifiedKey == "app_start" { return .appStart }
        
        
        return .homescreen
    }
}

enum CPAdPlacementActionType: String {
    case impression
    case click
}


struct CPAdPlacement {
    var type: CPAdPlacementType
    var impressionLink: String?
    var clickLink: String?
    
    init(aType: CPAdPlacementType, links: [String: Any]) {
        type = aType
        impressionLink = links["impression_link"] as? String
        clickLink = links["click_link"] as? String
    }
    
    func linkForAction(_ actionType: CPAdPlacementActionType) -> String? {
        return actionType == .impression ? impressionLink : clickLink
    }
}
