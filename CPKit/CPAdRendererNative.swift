//
//  CPAdVisualizer.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/12/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit


class CPAdRendererNative: CPAdRenderer, CPAdRendererProtocol {
    
    
    func renderAds(ads: [CPAdProtocol],
                   forPlacement placementType: CPAdPlacementType,
                   inView containerView: UIView) {
        
        if let theAds = ads as? [CPAd] {
            self.ads = theAds
        }
        self.placementType = placementType
        self.containerView = containerView
        
        containerView.subviews.forEach { $0.removeFromSuperview() }
        
        let aLabel = UILabel(frame: .zero)
        aLabel.translatesAutoresizingMaskIntoConstraints = false
        aLabel.textAlignment = .center
        aLabel.isHidden = true
        containerView.addSubview(aLabel)
        aLabel.constraintBottom(toView: containerView, constant: -68.0)
        aLabel.constraintLeft(toView: containerView)
        aLabel.constraintRight(toView: containerView)
        aLabel.constraintHeight(14)
        aLabel.font = UIFont(name: "Montserrat-Regular", size: 12)
        aLabel.textColor = UIColor.black.withAlphaComponent(0.3)
        aLabel.text = NSLocalizedString("dlMoreBanner", comment: "")
        titleLabel = aLabel
        
        let adsNumber: CGFloat = CGFloat(ads.count)
        let placeholderViewWidth = adsNumber * 44.0 + (adsNumber - 1) * 24.0
        let placeholderView = UIView(frame: CGRect.zero)
        placeholderView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(placeholderView)
        placeholderView.constraintBottom(toView: containerView, constant: -12.0)
        placeholderView.constraintCenterX(toView: containerView)
        placeholderView.constraintWidth(placeholderViewWidth)
        placeholderView.constraintHeight(44.0)
        
        for (idx, var ad) in ads.enumerated() {
            let adImageView = UIImageView(frame: CGRect.zero)
            adImageView.tag = idx
            adImageView.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(containerViewDidTouch(sender:)))
            tapGesture.numberOfTapsRequired = 1
            adImageView.addGestureRecognizer(tapGesture)
            guard let native = ad as? CPAdNativeProtocol else { return }
            
            if native.icon == nil {
                guard let assetOwner = ad as? CPAdAssetOwner,
                    let assetURLString = assetOwner.assetURLString,
                    let url = URL(string: assetURLString) else { return }
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url)
                    DispatchQueue.main.async {
                        adImageView.image = UIImage(data: data!)
                        self.titleLabel?.isHidden = false
                        if ad.dateVisualization == nil {
                            ad.dateVisualization = Date()
                            ad.cappingUsed += 1
                        }
                    }
                }
            } else {
                adImageView.image = native.icon
                self.titleLabel?.isHidden = false
                if ad.dateVisualization == nil {
                    ad.dateVisualization = Date()
                    ad.cappingUsed += 1
                }
            }
            adImageView.contentMode = .scaleAspectFill
            adImageView.layer.cornerRadius = 12
            adImageView.clipsToBounds = true
            
            let shadowView = UIView(frame: CGRect.zero)
            shadowView.translatesAutoresizingMaskIntoConstraints = false
            shadowView.clipsToBounds = false
            
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOpacity = 0.3
            shadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
            shadowView.layer.shadowRadius = 3
            
            placeholderView.addSubview(shadowView)
            shadowView.constraintWidth(44.0)
            shadowView.constraintHeight(44.0)
            shadowView.constraintTop(toView: placeholderView)
            let multiplier = CGFloat(idx * 2 + 1) / adsNumber
            shadowView.constraintCenterX(toView: placeholderView, multiplier:multiplier)
            
            adImageView.translatesAutoresizingMaskIntoConstraints = false
            shadowView.addSubview(adImageView)
            adImageView.constraintLeft(toView: shadowView)
            adImageView.constraintRight(toView: shadowView)
            adImageView.constraintTop(toView: shadowView)
            adImageView.constraintBottom(toView: shadowView)
        }
        
        
        
        //Track impressions
        let links = ads.compactMap { $0.placements[placementType]?.impressionLink }
        trackImpressionLinks(links, pendingImpressionLinks: pendingImpressionLinks, withRestManager: restManager, success: { (aLink) in
            if self.pendingImpressionLinks.contains(aLink) {
                self.pendingImpressionLinks.remove(at: self.pendingImpressionLinks.firstIndex(of: aLink)!)
            }
        }) { (aLink) in
            if !self.pendingImpressionLinks.contains(aLink) {
                self.pendingImpressionLinks.append(aLink)
            }

        }
    }
}

