//
//  CPAdMedia.swift
//  CPKit
//
//  Created by Olha Honcharuk on 7/3/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import Foundation

class CPAdMedia: NSObject {
    var assetType: CPAssetType = .undefined
    var assetURLString: String?
    var callToAction: String?
    var appName: String?
    var title: String?
    var urgentStamp: Int = 0
    var fileName: String?
    var pathExtension: String?
    
    init(json: [String:Any]) {
        let assetTypeStr = json["asset_type"] as? String ?? ""
        if let anAssetType = CPAssetType(rawValue: assetTypeStr) {
            assetType = anAssetType
        }
        assetURLString = json["asset_url"] as? String
        callToAction = json["call_to_action"] as? String
        appName = json["app_name"] as? String
        title = json["title"] as? String
        urgentStamp = json["urgent_stamp"] as? Int ?? 0
    }
    
    var assetURL: URL? {
        guard let assetString = assetURLString else { return nil }
        return URL(string:assetString)
    }
    
    var assetFileId: String? {
        guard let assetString = assetURLString else { return nil }
        let escapedString = assetString.trimmingCharacters(in: .whitespacesAndNewlines)
        return URL(string: escapedString)?.deletingPathExtension().lastPathComponent
    }
    
    func isAssetDownloaded() -> Bool {
        return fileName != nil
    }
    
    
}
