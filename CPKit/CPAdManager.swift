//
//  CPNetwork.swift
//  CPKit
//
//  Created by Olha Honcharuk on 6/26/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import Foundation
import UIKit

enum CPKitStorageDataType: String {
    case data
    case date
    case cacheDuration
    
    func key() -> String {
        return "cpkit" + self.rawValue.uppercased()
    }
}

@objc public class CPAdManager: NSObject {
    var cappingDuration: Int = 0 // It defines the duration an ad who reach its capping is unable to be displayed (in hours)
    var cacheDuration: Int = 0 // It defines the duration (in hours) we keep the JSON in cache, in order to prevent to retrieve a new one if we already retrieved it recently.
    var cacheCreationDate: Date?
    var versionMin: Int = 0 // It prevents a CPKit without a sufficient version number to use this JSON.
    var ads: [CPAdProtocol]?
    
    var rest: CPRestManager?
    
    var appId: String
    var gameId: String
    
    var data: Data?
    
    var sessions = [CPAdFormat: CPAdSession]()
    
    @objc public init(appId: String, gameId: String) {
        self.appId = appId
        self.gameId = gameId
        
        super.init()
        
        fetchSettingsFromTheStorage()
        
        if adsConfigurationIsOutdated() || !isDataPresentInTheStorage() {
            requestAds(forAppId: self.appId, gameId: self.gameId)
        } else {
            fetchDataFromTheStorage()
            guard let data = data else { return }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
                    parseConfigurationJSON(json)
                    if let theAds = ads {
                        let filesToKeep = theAds.compactMap { (ad) -> String? in
                            if let assetOwner = ad as? CPAdAssetOwner {
                                return assetOwner.assetName
                            }
                            return nil
                        }
                        cleanFilesAvoiding(filesToKeep: filesToKeep)
                    }
                    
                }
            } catch {
                //print(" CPKIT: *** Something went wrong *** ")
            }
        }
    }
    
    @objc public func showNativeInContainer(_ containerView: UIView,
                                            placementType: CPAdPlacementType) -> Bool {
        return showAds(adsFormat: .native,
                       containerView: containerView,
                       placementType: placementType)
    }
    
    @objc public func showInterstitialInContainer(_ containerView: UIView?,
                                                  placementType: CPAdPlacementType) -> Bool {
        
        return showAds(adsFormat: .fullScreen,
                       containerView: containerView,
                       placementType: placementType)
    }
    
    @objc public func showSquareAdInContainer(_ containerView: UIView?,
                                              placementType: CPAdPlacementType) -> Bool {
        return showAds(adsFormat: .square,
                       containerView: containerView,
                       placementType:  placementType)
    }
    
    @objc public func canShowInterstitial() -> Bool {
        guard let theAds = ads else { return false }
        let targetAds = theAds.filter { $0.format == CPAdFormat.fullScreen }
        return targetAds.count > 0
    }
    
    
}

extension CPAdManager {
    
    func requestAds(forAppId appId: String, gameId: String) {
        rest = CPRestManager()
        guard let aRestManager = rest else { return }
        
        guard let configurationURL = URL(string: CPDefines.CONTENT_URL) else { return }
        aRestManager.urlQueryParameters.add(value: gameId, forKey: "gameId")
        aRestManager.requestHTTPHeaders.add(value: CPDefines.applicationJSON, forKey: CPDefines.contentType)
        
        // Add body params
        // Device UUID
        if let deviceUUID = CPDefines.deviceUUID {
            aRestManager.httpBodyParameters.add(value: deviceUUID, forKey: "udid")
        }
        
        aRestManager.httpBodyParameters.add(value: appId, forKey: "appId")
        
        
        //Device type
        let deviceType = UIDevice.current.userInterfaceIdiom == .pad ? "tablet" : "phone"
        aRestManager.httpBodyParameters.add(value: deviceType, forKey: "device")
        
        //Country
        if let country = CPDefines.country {
            aRestManager.httpBodyParameters.add(value: country, forKey: "country")
        }
        //Language
        aRestManager.httpBodyParameters.add(value: CPDefines.language, forKey: "lang")
        
        aRestManager.makeRequest(toURL: configurationURL, withHttpMethod: .post) {  [weak self] (results) in
            if let receivedConfigurationData = results.data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: receivedConfigurationData, options: []) as? [String:Any],
                        let self = self {
                        self.data = receivedConfigurationData
                        self.parseConfigurationJSON(json)
                        if self.ads?.count ?? 0 > 0 {
                            self.saveSettingsToTheStorage()
                            self.saveDataToTheStorage()
                        }
                    }
                } catch {
                    print(" CPKIT: *** Something went wrong PARSING data *** ")
                }
            } else {
                if let self = self {
                    //print(" CPKIT: *** Try to Request configuration JSON *** ")
                    self.requestAds(forAppId: self.appId, gameId: self.gameId)
                }
            }
        }
    }
    
    func adsConfigurationIsOutdated() -> Bool {
        guard let cacheCreationDate = cacheCreationDate else { return true }
        
        let timeInterval = cacheCreationDate.timeIntervalSinceNow
        let hoursKoef: Double = 60.0 * 60.0
        let hoursInterval = fabs(timeInterval/hoursKoef)
        return hoursInterval > Double(cacheDuration)
    }
    
    func showAds(adsFormat: CPAdFormat, containerView: UIView?, placementType: CPAdPlacementType) -> Bool {
        guard let theAds = ads else {
            return false
        }
        
        let targetAds = theAds.filter {$0.format == adsFormat}
        
        var adSession = sessions[adsFormat]
        if adSession == nil {
            adSession = CPAdSession(format: adsFormat,
                                    creationDate: Date.init(timeIntervalSinceNow: 0.0))
            sessions[adsFormat] = adSession
        }
        adSession?.showAds(targetAds,
                           format: adsFormat,
                           placementType: placementType,
                           inContainerView: containerView)
        return true
    }
    
    
}

extension CPAdManager {
    
    func fetchSettingsFromTheStorage() {
        let storage = UserDefaults.standard
        var sDataType = CPKitStorageDataType.cacheDuration
        if let aCacheDuration = storage.object(forKey: sDataType.key()) as? Int {
            cacheDuration = aCacheDuration
        }
        sDataType = .date
        if let cpKitConfigurationDate = storage.object(forKey: sDataType.key()) as? Date {
            cacheCreationDate = cpKitConfigurationDate
        }
    }
    
    func saveSettingsToTheStorage() {
        let storage = UserDefaults.standard
        var sDataType = CPKitStorageDataType.date
        storage.set(Date(), forKey: sDataType.key())
        sDataType = .cacheDuration
        storage.set(cacheDuration, forKey: sDataType.key())
    }
    
    func saveDataToTheStorage() {
        guard let data = data else { return }
        let storage = UserDefaults.standard
        let sDataType = CPKitStorageDataType.data
        storage.set(data, forKey: sDataType.key())
    }
    
    func fetchDataFromTheStorage() {
        let storage = UserDefaults.standard
        let sDataType = CPKitStorageDataType.data
        if let aData = storage.object(forKey: sDataType.key()) as? Data {
            data = aData
        }
    }
    
    func isDataPresentInTheStorage() -> Bool {
        let storage = UserDefaults.standard
        let sDataType = CPKitStorageDataType.data
        return storage.object(forKey: sDataType.key()) as? Data != nil
    }
    
    func parseConfigurationJSON(_ json: [String:Any]) {
        guard let results = json["results"] as? [String: Any] else { return  }
        if let aCapDuration = results["capping_duration"] as? Int {
            cappingDuration = aCapDuration
        }
        if let aCacheDuration = results["cache_duration"] as? Int {
            cacheDuration = aCacheDuration
            //TODO: save timestamp of the cache to the defaults
        }
        if let aVersionMin = results["version_min"] as? Int {
            versionMin = aVersionMin
        }
        guard let theAds = results["ads"] as? [Dictionary<String,Any>] else { return }
        ads = theAds.map { CPAd(json:$0) }
    }
    
    func cleanFilesAvoiding(filesToKeep: Array<String>) {
            
        // Get the document directory url
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl.appendPathComponent("cpkit/files/")

        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])

            for fileNameUrl in directoryContents {
                let lastPathComponent = fileNameUrl.deletingPathExtension().lastPathComponent
                if !filesToKeep.contains(lastPathComponent) {
                    do {
                        try FileManager.default.removeItem(at: fileNameUrl)
                    } catch {
                        
                    }
                }
            }
        } catch _ as NSError {
        }
    }
}

