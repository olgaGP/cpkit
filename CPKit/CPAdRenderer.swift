//
//  CPAdRenderer.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/15/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

protocol TrackingManager {
    var storeDelegate: CPAdStoreDelegate? { get set }
}

protocol CPAdStoreDelegate {
    func willShowStoreViewIn(_ trackingManager: TrackingManager?)
    func didShowStoreViewIn(_ trackingManager: TrackingManager?)
    func didCloseStoreViewIn(_ trackingManager: TrackingManager?)
    func didFailStoreViewIn(_ trackingManager: TrackingManager?)
}

class CPAdRenderer: NSObject {
    weak var containerView: UIView?
    var titleLabel: UILabel?
    var ads: [CPAdProtocol]?
    var restManager: CPRestManager?
    var placementType: CPAdPlacementType = .homescreen
    var pendingImpressionLinks = [String]()
    let storeProductViewController = SKStoreProductViewController()
    
    var trackingManager: TrackingManager?
    
    @objc func containerViewDidTouch(sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag,
            let ads = ads,
            isStoreKitAvailable() else { return }
        
        //Open Store
        guard let ad = ads[tag] as? CPAd,
            let appId = ad.appId else { return }
        openStore(forIdentifier: appId)
        
        //Track click
        guard let placement = ad.placements[placementType]  else { return }
        
        restManager = CPRestManager()
        trackClickAction(action: .click, for: placement, withRestManager: restManager)
    }
    
    func isStoreKitAvailable() -> Bool {
        for bundle in Bundle.allFrameworks {
            if ((bundle.classNamed("SKStoreProductViewController")) != nil) {
                return true
            }
        }
        return false
    }
    
    func openStore(forIdentifier identifier: String) {
        
        // let storeProductViewController = SKStoreProductViewController()
        storeProductViewController.delegate = self
        
        // Create a product dictionary using the App Store's iTunes identifer.
        /*let parametersDict : [String:Any] = [SKStoreProductParameterITunesItemIdentifier: String(identifier), SKStoreProductParameterCampaignToken: "Crosspromo_StoreView", SKStoreProductParameterProviderToken: "2212542"]*/
        var parametersDict = [String:Any]()
        if #available(iOS 8.3, *) {
            parametersDict = [SKStoreProductParameterITunesItemIdentifier: String(identifier), SKStoreProductParameterCampaignToken: "Crosspromo_StoreView", SKStoreProductParameterProviderToken: "2212542"]
        } else {
            // Fallback on earlier versions
            parametersDict = [SKStoreProductParameterITunesItemIdentifier: String(identifier), SKStoreProductParameterCampaignToken: "Crosspromo_StoreView"]
        }
        
        /* Attempt to load it, present the store product view controller if success
         and print an error message, otherwise. */
        trackingManager?.storeDelegate?.willShowStoreViewIn(trackingManager)
        storeProductViewController.loadProduct(withParameters: parametersDict, completionBlock: { (status: Bool, error: Error?) -> Void in
            if status {
                self.trackingManager?.storeDelegate?.didShowStoreViewIn(self.trackingManager)
                // UIApplication.shared.keyWindow?.rootViewController?.present(TrackingManager.shared.storeProductViewController, animated: true, completion: nil)
            }
            else {
                self.trackingManager?.storeDelegate?.didFailStoreViewIn(self.trackingManager)
                if error != nil {
//                    GPCPLogs.log("Failed to show the SKStoreProductViewController : \(String(describing: error.localizedDescription))", level: .high)
                }}})
        UIApplication.shared.keyWindow?.rootViewController?.present(storeProductViewController, animated: true, completion: nil)
    }
    
    func openURL(_ url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func trackImpressionLinks(_ impressionLinks: [String],
                              pendingImpressionLinks: [String],
                              withRestManager restManager: CPRestManager?,
                              success: @escaping (String) -> (),
                              failure: @escaping(String) -> ()) {
        
        guard let aRestManager = restManager else { return }
        
        let linksToTrack = impressionLinks + pendingImpressionLinks
        
        linksToTrack.forEach { (aLink) in
            if let url = URL(string: aLink) {
                aRestManager.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
                    if results.error == nil {
                        success(aLink)
                    } else {
                        failure(aLink)
                    }
                }
            }
        }
    }
    
    func trackClickAction(action: CPAdPlacementActionType,
                          for placement: CPAdPlacement,
                          withRestManager restManager: CPRestManager?) {
        
        guard let trackLink = placement.linkForAction(action) else { return }
        
        //"?advertising_id=" + ASIdentifierManager.shared().advertisingIdentifier.uuidString
        
        
        guard let aRestManager = restManager,
            let trackURL = URL(string: trackLink) else { return }
        
        aRestManager.makeRequest(toURL: trackURL, withHttpMethod: .get) { (results) in
            if results.error == nil {
                print("Tenjin track \(action) for id")
            } else {
                print("Tenjin Failed to track \(action) for id")
            }
        }
    }
}

extension CPAdRenderer: SKStoreProductViewControllerDelegate {
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.presentingViewController?.dismiss(animated: true, completion: nil)
        trackingManager?.storeDelegate?.didCloseStoreViewIn(trackingManager)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
