//
//  CPAdRendererProtocol.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/15/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit

protocol CPAdRendererProtocol {
    func renderAds(ads: [CPAdProtocol],
                   forPlacement placementType: CPAdPlacementType,
                   inView containerView: UIView)
    
    var ads: [CPAdProtocol]? { get }
}

extension CPAdRendererProtocol {
    
    
    
    
    
}
