//
//  CPAdRendererSquare.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/15/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit


class CPAdRendererSquare: CPAdRenderer, CPAdRendererProtocol {
    
    var player: AVPlayer?
    var playerViewController: AVPlayerViewController?
    
    func renderAds(ads: [CPAdProtocol],
                   forPlacement placementType: CPAdPlacementType,
                   inView containerView: UIView) {
        
        guard let ad = ads.first as? CPAdAssetOwner,
            let videoURL = ad.assetFileURL else { return }
            
        containerView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        player = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = containerView.bounds
        containerView.layer.addSublayer(playerLayer)

        player?.play()
    }
    
}
