//
//  CPAd.swift
//  CPKit
//
//  Created by Olha Honcharuk on 6/27/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import Foundation
import UIKit

enum CPAdPriority: Int {
    case undefined = -1
    case low = 0
    case medium = 1
    case high = 2
}


enum CPAdFormat: String {
    case undefined
    case square = "square"
    case fullScreen = "fullscreen"
    case native = "native"
}

enum CPAssetType: String {
    case undefined
    case media
    case icon
}

protocol CPAdAssetOwner {
    var assetName: String? { get }
    var assetExtension: String? { get }
    var assetFileURL: URL? { get }
    var assetURLString: String? { get }
}

protocol CPAdNativeProtocol {
    var icon: UIImage? {get}
    var title: String? {get}
}

class CPAd: CPAdProtocol {
    var format: CPAdFormat?
    var dateVisualization: Date?
    var capping: Int?
    var cappingUsed: Int = 0
    
    var appStoreLink: String? {
        guard let media = media else { return nil }
        return media.callToAction
    }
    var id: Int?
    var appId: String?
    var priority: CPAdPriority = .undefined
    
    var versionMin: Int?
    
    var placements = [CPAdPlacementType:CPAdPlacement]()
    var media: CPAdMedia?
    private var restManager: CPRestManager?
    
    init(json: [String:Any]) {
        id = json["id"] as? Int
        appId = json["app_id"] as? String
        if let aPriority = CPAdPriority(rawValue: json["priority"] as? Int ?? -1) {
            priority = aPriority
        }
        capping = json["capping"] as? Int
        versionMin = json["version_min"] as? Int
        if let aFormat = CPAdFormat(rawValue:json["format"] as? String ?? "undefined") {
            format = aFormat
        }
        if let thePlacements = json["placement"] as? [String:[String:String]] {
            for (key, value) in thePlacements {
                let aType = CPAdPlacementType.placementTypeFromKey(key)
                let aPlacement = CPAdPlacement(aType: aType, links: value)
                placements[aType] = aPlacement
            }
        }
        if let theMedia = json["media"] as? [String:Any] {
            media = CPAdMedia(json: theMedia)
        }
        
        downloadAssetsIfNeeded()
    }
}


extension CPAd: CPAdNativeProtocol {
    
    public var title: String? {
        guard let media = media else { return nil }
        return media.appName
    }
    
    public var icon: UIImage? {
        guard let media = media,
            let fileName = media.fileName,
            let pathExtension = media.pathExtension else { return nil }
        
        let assetCacheString = fileName.inDocumentDirectory().stringByAddingPathExtension(pathExtension)
        let image = UIImage(contentsOfFile: assetCacheString)
        return image
    }
}

extension CPAd: CPAdAssetOwner {
    
    var assetFileURL: URL? {
        guard let assetName = assetName,
            let assetExtension = assetExtension else { return nil }
        
        let fm = FileManager.default
        var videoURL = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        videoURL.appendPathComponent("cpkit/files/")
        videoURL = videoURL.appendingPathComponent(assetName + "." + assetExtension)
        return videoURL
    }
    
    var assetName: String? {
        guard let media = media else { return nil }
        return media.fileName
    }
    
    var assetExtension: String? {
        guard let media = media else { return nil }
        return media.pathExtension
    }
    
    public var assetURLString: String? {
        return media?.assetURLString
    }
    
}

protocol CPAdProtocol {
    var id: Int? { get }
    var format: CPAdFormat? { get set }
    var dateVisualization: Date? { get set }
    var placements: [CPAdPlacementType:CPAdPlacement] { get }
    
    var capping: Int? { get }
    var cappingUsed: Int { get set }
    var isValidCapping: Bool { get }
    
    mutating func invalidateCapping()
}

extension CPAdProtocol {
    var isValidCapping: Bool {
        guard let dateVisualization = dateVisualization,
            let capping = capping else {
                return true
        }
        
        let timeInterval = dateVisualization.timeIntervalSinceNow
        let hoursKoef: Double = 60.0 * 60.0
        let hoursInterval = fabs(timeInterval/hoursKoef)
        if hoursInterval < Double(capping) {
            return cappingUsed < capping
        } else {
            return false
        }
    }
    
    mutating func invalidateCapping() {
        guard let dateVisualization = dateVisualization,
            let capping = capping else { return }
        
        let timeInterval = dateVisualization.timeIntervalSinceNow
        let hoursKoef: Double = 60.0 * 60.0
        let hoursInterval = fabs(timeInterval/hoursKoef)
        if hoursInterval > Double(capping) {
            self.dateVisualization = nil
            self.cappingUsed = 0
        }
    }
}


extension CPAd {
    
    func downloadAssetsIfNeeded() {
        guard let media = media,
            !media.isAssetDownloaded(),
            let urlString = media.assetURLString,
            let fileId = media.assetFileId,
            let pathExtension = URL(string: urlString)?.pathExtension,
            let requestURL = URL(string: urlString) else { return }
        
        let filePath = fileId.inDocumentDirectory().stringByAddingPathExtension(pathExtension)
        
        //Check if file does not exist, else do nothing and return
        guard !FileManager.default.fileExists(atPath: filePath) else {
            media.fileName = fileId
            media.pathExtension = pathExtension
            return
        }
        
        let restManager = CPRestManager()
        let task = CPDownloadResourceRepetitiveTask(restManager: restManager, url: requestURL)
        task.createRequestWithRetryAndRun(retryCount: 10,
                                          failure: { (error) in
            print(error)
        }) { (data) in
            do {
                var directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                directoryURL.appendPathComponent("cpkit/files/");
                
                try FileManager.default.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
            } catch {
            }
            if FileManager.default.createFile(atPath: filePath,
                                              contents: data,
                                              attributes: nil) {
                //DOWNLOAD FINI!
                DispatchQueue.main.async() {
                    self.media?.fileName = fileId
                    self.media?.pathExtension = pathExtension
                }
            }
        }
    }
}
