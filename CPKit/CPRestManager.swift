//
//  RestManager.swift
//  CPKit
//
//  Created by Olha Honcharuk on 6/29/19.
//  Copyright © 2019 Olha Honcharuk. All rights reserved.
//

import Foundation

// Main body is for implementation of private and public methods
class CPRestManager {
    
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    var requestHTTPHeaders = CPRestEntity()
    var urlQueryParameters = CPRestEntity()
    var httpBodyParameters = CPRestEntity()
    
    var httpBody: Data?
    
    func makeRequest(toURL url: URL,
                     withHttpMethod httpMethod: CPHttpMethod,
                     completion: @escaping(_ result: CPResults) -> Void) {
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let targetURL = self?.addURLQueryParameters(toURL: url)
            let httpBody = self?.getHttpBody()
            guard let request = self?.prepareRequest(withURL: targetURL,
                                                     httpBody: httpBody,
                                                     httpMethod: httpMethod) else {
                                                        completion(CPResults(withError: CustomError.failedToCreateRequest))
                                                        return
            }
            //let sessionConfiguration = URLSessionConfiguration.default
            //let session = URLSession(configuration: sessionConfiguration)
            let task = self?.session.dataTask(with: request) { (data, response, error) in
                completion(CPResults(withData: data,
                                   response: CPResponse(fromURLResponse: response),
                                   error: error))
            }
            task?.resume()
        }
    }
    
    func getData(fromURL url: URL, completion: @escaping(_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data else { completion(nil, response, error); return }
                completion(data, nil, nil)
            })
            task.resume()
        }
    }
    
    private func addURLQueryParameters(toURL url: URL) -> URL {
        if urlQueryParameters.totalItems() > 0 {
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            var queryItems = [URLQueryItem]()
            for (key, value) in urlQueryParameters.allValues() {
                let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                queryItems.append(item)
            }
            urlComponents.queryItems = queryItems
            guard let updatedURL = urlComponents.url else { return url }
            return updatedURL
        }
        return url
    }
    
    private func getHttpBody() -> Data? {
        guard  let contentType = requestHTTPHeaders.value(forKey: "Content-Type") else { return nil }
        if contentType.contains("application/json") {
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [.prettyPrinted])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            let bodyString = httpBodyParameters.allValues().map { "\($0)=\(String(describing: $1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)))" }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    private func prepareRequest(withURL url: URL?,
                                httpBody: Data?,
                                httpMethod: CPHttpMethod) -> URLRequest? {
        
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        for (header, value) in requestHTTPHeaders.allValues() {
            request.setValue(value, forHTTPHeaderField: header)
        }
        request.httpBody = httpBody
        return request
    }
    
}

// Extension is for implementation of inner types: structs and enums.
extension CPRestManager {
    
    // HttpMethod
    // Enumeration represents various HTTP methods
    enum CPHttpMethod: String {
        case get
        case post
        case put
        case patch
        case delete
    }
    
    struct CPRestEntity {
        private var values: [String: String] = [:]
        
        mutating func add(value: String, forKey key: String) {
            values[key] = value
        }
        
        func value(forKey key: String) -> String? {
            return values[key]
        }
        
        func allValues() -> [String: String] {
            return values
        }
        
        func totalItems() -> Int {
            return values.count
        }
    }
    
    struct CPResponse {
        var response: URLResponse?
        var httpStatusCode: Int = 0
        var headers = CPRestEntity()
        
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    struct CPResults {
        var data: Data?
        var response: CPResponse?
        var error: Error?
        
        init(withData data: Data?, response: CPResponse?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
        }
        
        init(withError error: Error) {
            self.error = error
        }
    }
    
    enum CustomError: Error {
        case failedToCreateRequest
    }
    
}

extension CPRestManager.CustomError: LocalizedError {
    
    public var localizedDescription: String {
        switch self {
        case .failedToCreateRequest: return NSLocalizedString("Unable to create the URLRequest object", comment: "")
        }
    }
}
