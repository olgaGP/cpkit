//
//  CPKit.h
//  CPKit
//
//  Created by Olha Honcharuk on 7/24/19.
//  Copyright © 2019 GreenPandaGames. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CPKit.
FOUNDATION_EXPORT double CPKitVersionNumber;

//! Project version string for CPKit.
FOUNDATION_EXPORT const unsigned char CPKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CPKit/PublicHeader.h>


