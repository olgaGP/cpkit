//
//  CP.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/15/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

class CPAdRendererFullscreen: CPAdRenderer, CPAdRendererProtocol {
    
    var player: AVPlayer?
    var playerViewController: AVPlayerViewController?
    
    func renderAds(ads: [CPAdProtocol],
                   forPlacement placementType: CPAdPlacementType,
                   inView containerView: UIView) {
        
        guard let ad = ads.first as? CPAdAssetOwner,
            let videoURL = ad.assetFileURL else { return }
        
        let player = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: player)
        
        let blackView = UIView(frame: .zero)
        blackView.backgroundColor = UIColor.black
        blackView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(blackView)
        blackView.constraintLeft(toView: containerView)
        blackView.constraintRight(toView: containerView)
        blackView.constraintTop(toView: containerView)
        blackView.constraintBottom(toView: containerView)
        
        playerLayer.frame = CGRect(x: 0, y: 0, width: containerView.bounds.width, height: containerView.bounds.height)
        blackView.layer.addSublayer(playerLayer)
        addCloseButtonOnView(blackView)
        player.play()
    }
}

extension CPAdRendererFullscreen {
    
    func addCloseButtonOnView(_ aView: UIView) {
        let button = CPTimerButton(type: .custom)
        button.layer.cornerRadius = 14
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.addTarget(self, action: #selector(closeButtonDidTouch(sender:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.white
       
        aView.addSubview(button)
        button.constraintTop(toView: aView, constant: 10)
        button.constraintRight(toView: aView, constant: -10)
        button.constraintWidth(28)
        button.constraintHeight(28)
        
        button.timerStart()
    }
    
    @objc func closeButtonDidTouch(sender: UIButton) {
        sender.superview?.removeFromSuperview()
    }
}
