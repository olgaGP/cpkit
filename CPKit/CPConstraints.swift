//
//  CPConstraints.swift
//  Solitaire
//
//  Created by Olha Honcharuk on 7/12/19.
//  Copyright © 2019 Classic Games Produciton. All rights reserved.
//

import Foundation
import UIKit

@objc extension UIView {
    
    func constraintLeft(toView: UIView, constant: CGFloat = 0) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1, constant: constant)
        toView.addConstraint(aConstraint)
    }
    func constraintRight(toView: UIView, constant: CGFloat = 0) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1, constant: constant)
        toView.addConstraint(aConstraint)
    }
    func constraintTop(toView: UIView, constant: CGFloat = 0) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: constant)
        toView.addConstraint(aConstraint)
    }
    func constraintBottom(toView: UIView, constant: CGFloat = 0) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: constant)
        toView.addConstraint(aConstraint)
    }
    func constraintCenterX(toView: UIView, constant: CGFloat = 0) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: constant)
        toView.addConstraint(aConstraint)
    }
    
    func constraintCenterX(toView: UIView, multiplier: CGFloat) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: multiplier, constant: 0)
        toView.addConstraint(aConstraint)
    }
    
    func constraintCenterY(toView: UIView) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        toView.addConstraint(aConstraint)
    }
    
    func constraintCenterY(toView: UIView, constant: CGFloat) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: constant)
        toView.addConstraint(aConstraint)
    }
    
    func constraintWidth(_ aWidth: CGFloat) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: aWidth)
        self.addConstraint(aConstraint)
    }
    func constraintHeight(_ aHeight: CGFloat) {
        let aConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: aHeight)
        self.addConstraint(aConstraint)
    }
}
