//
//  CPTimerButton.swift
//  SpiderSolitaire
//
//  Created by Olha Honcharuk on 7/24/19.
//  Copyright © 2019 Green Panda. All rights reserved.
//

import UIKit

class CPTimerButton: UIButton {
    private var timer: Timer?
    var timeLeft = 5
    
    func timerStart() {
        setTitle("\(timeLeft)", for: .normal)
        
        isEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimeFires), userInfo: nil, repeats: true)
        if let aTimer = timer {
            RunLoop.current.add(aTimer, forMode: .common)
        }
    }
    
    @objc func onTimeFires() {
        timeLeft -= 1
        setTitle("\(timeLeft)", for: .normal)
        
        if timeLeft <= 0 {
            timer?.invalidate()
            timer = nil
            isEnabled = true
            
            let cpkitBundle = Bundle(for: CPTimerButton.self)
            guard let crossImage = UIImage(named: "custom-icn-btn-close", in: cpkitBundle, compatibleWith: nil) else { return }
            setTitle("", for: .normal)
            setImage(crossImage, for: .normal)
            
        }
    }
}
